	/**
	 * Sample React Native App
	 * https://github.com/facebook/react-native
	 * @flow
	 */

	import React, { Component } from 'react';

	import {
	  AppRegistry,
	  Image,
	  StyleSheet,
	  ListView,
	  DrawerLayoutAndroid,
	  Text,
	  TouchableHighlight,
	  ToastAndroid ,
	  TextInput,
	 ParseReact,
	  DatePickerAndroid,
	  TouchableWithoutFeedback,
	  TouchableNativeFeedback,
	  View
	} from 'react-native';

	//var Parse = require('parse/react-native');
	//var ParseComponent = ParseReact.Component(React);
		
	//Parse.initialize(
	//  'Nlv14hb32fwBopAVXMzF6LRq1kom5TVC2PGOGxVt',
	//  'apDFnCB6KyPxie54HxjRQQ4oCwRhFFM1D2TGylx7'
	//);

	class TodoApp extends Component {
	 mixins: [ParseReact.Mixin]
		//console.warn('todoApp')
		
	 constructor(props) {
		super(props);
		this.state = {
		  dataSource: new ListView.DataSource({
			rowHasChanged: (row1, row2) => row1 !== row2,
		  }),
		  type: 'home',
		  tname: '' ,
		  tdetails: '',
		}
		}
	  /* observe() {
		// Subscribe to all Comment objects, ordered by creation date
		// The results will be available at this.data.comments
		var k = (new Parse.Query('todo')).ascending('createdAt')
		console.log(k)
		//return (new Parse.Query('todo')).ascending('createdAt')
		return {
		comments : (new Parse.Query('todo')).ascending('createdAt')
		};
	  }*/
		componentDidMount() {
	   // this.fetchData();
	  }
	  
	  
	  

	  maybeRenderElement() {
	   if(this.state.type=='home')
	  {
		 
	return <View  style={{flexDirection:'column'}}>
		  <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5}}>Task Name</Text>
	   <TextInput
	   style={{height: 40, fontSize: 20 ,borderColor: '#004D40', borderWidth: 0.1}}
	   ref= "tname"
	  onChangeText={(tname) => this.setState({tname})}
		value={this.state.tname}
	  />
	  <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5}}>Task Description</Text>
	   <TextInput
		style={{height: 40, fontSize: 20 ,borderColor: '#004D40', borderWidth: 0.1}}
		 ref= "tdetails"
		onChangeText={(tdetails) => this.setState({tdetails})}
		value={this.state.tdetails}
	  />
		  <TouchableHighlight onPress={() => this.addNewTask()  }>

		  <View style={{width: 150, height: 50, backgroundColor: '#004D40', marginTop:25 ,alignSelf :'center'}}>
			<Text style={{fontSize: 20 ,color:'#ffffff' ,alignSelf :'center'}}>Add Task! </Text>
		  </View>
		</TouchableHighlight>
		</View >
		
		
		/*<Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5}}>Date</Text>
		 
		 <TouchableWithoutFeedback
				onPress={this.showPicker.bind(this, 'min', {
				  date: this.state.minDate,
				  minDate: new Date(),
				})}>
				 <View >
				<Text style={{color:'#004D40',fontSize: 20 }}>{this.setState.newState}</Text>
				</View>
			  </TouchableWithoutFeedback>
			  <TouchableHighlight onPress={() => this.addNewTask()  }>

		  <View style={{width: 150, height: 50, backgroundColor: '#004D40', marginTop:25 ,alignSelf :'center'}}>
			<Text style={{fontSize: 20 ,color:'#ffffff' ,alignSelf :'center'}}>Add Task! </Text>
		  </View>
		</TouchableHighlight>*/
	  }
	  else if(this.state.type=='pending')
	  {
		 // this.setState({ rows: 
		  
		//  return (new Parse.Query('todo')).ascending('createdAt')
	   
		   return <ListView
			dataSource={this.state.dataSource}
			renderRow={this.renderMovie}
		   
		  />


		  
	  }
	  else if(this.state.type=='completed')
	  {
		 // this.setState({ rows: 
			 return <ListView
			dataSource={this.state.dataSource}
			renderRow={this.renderMovie}
		   
		  />
		   


		  
	  }
	  else{
		   return <ListView
			dataSource={this.state.dataSource}
			renderRow={this.renderMovie}
		   
		  />
		  

		  
	  }
	  
	  }
		
	  render() {
	  var navigationView = (
		<View style={{flex: 1, backgroundColor: '#80CBC4'}}>
		  <View style={{flex: 1,flexDirection: 'column' , alignItems: 'center', justifyContent: 'center',marginTop:70 }}> 
		   <TouchableHighlight onPress={() => this.homeTask()  }>
	  <Text style={{color:'#ffffff' , fontSize: 20,marginBottom: 8,marginTop: 8}}>Home</Text>
	   </TouchableHighlight>
		 <TouchableHighlight onPress={() => this.ALLTask()  }>
		<Text style={{color:'#ffffff' ,fontSize: 20,marginBottom: 8,marginTop: 8}}>All</Text>
		</TouchableHighlight>
		 <TouchableHighlight onPress={() => this.PendingTask()  }>
	  <Text style={{color:'#ffffff',fontSize: 20,marginBottom: 8,marginTop: 8}}>Pending</Text>
	  </TouchableHighlight>
	 <TouchableHighlight onPress={() => this.CompletedTask() } >
	  <Text style={{color:'#ffffff',fontSize: 20,marginBottom: 8,marginTop: 8}}>Completed</Text>
	  </TouchableHighlight>
		  
		  </View>
		</View>
	  );
	  
	 
	 
	  
	  
			return (
			 <DrawerLayoutAndroid
		  drawerWidth={300}
		  drawerPosition={DrawerLayoutAndroid.positions.Left}
		  renderNavigationView={() => navigationView}>
		 <View style={{flex: 1 , flexDirection: 'column' ,backgroundColor: '#009688'}}>
		
		  <Text style={{color:'#ffffff',fontSize: 30,marginTop: 18 ,alignSelf :'center'}}>TO DO LIST</Text>
		 
		 </View>
			<View style={{flex: 4 ,backgroundColor: '#E0F7FA' ,flexDirection:'column' ,padding:20}}>
			  {this.maybeRenderElement()}
			  </View>
			   </DrawerLayoutAndroid>
	 );


	}
	  showPicker(stateKey, options) {
		try {
		  var newState = {};
		  var {action, year, month, day} =  DatePickerAndroid.open(options);
		  if (action === DatePickerAndroid.dismissedAction) {
			newState[stateKey + 'Text'] = 'dismissed';
		  } else {
			var date = new Date(year, month, day);
			newState[stateKey + 'Text'] = date.toLocaleDateString();
			newState[stateKey + 'Date'] = date;
		  }
		  this.setState(newState);
		  this.setState.minText =date.toLocaleDateString()
		  
		} catch ({code, message}) {
		  console.warn(`Error in example '${stateKey}': `, message);
		}
	  }


	 addNewTask() {
		 
	 fetch("https://api.parse.com/1/classes/todo", {
		 
	 method: "POST",
	 body: JSON.stringify({taskname: this.state.tname, taskdetail: this.state.tdetails, status: "pending"}) ,
	  headers: {
		"X-Parse-Application-Id": "Nlv14hb32fwBopAVXMzF6LRq1kom5TVC2PGOGxVt",
		"X-Parse-REST-API-Key": "tfkJINQzZWK8xRuWVK56UpJsyCVvKgwwTYHuWx56"
	  }
	})
	  .then((response) => response.json())
	  .then((responseData) => {
			this.setState({ type: 'all' })
	ToastAndroid.show('task added', ToastAndroid.SHORT)
	  })
	  .catch(function(error) {
		console.log(error)
	  })
	 .done();
		 
		 
		 
		   
		 
	 } 
	 
	  
		renderMovie(movie) {
	  var datec = movie.createdAt.split('T')
		
		return (
		<View style={{flexDirection:'column'}}>
		  <View  style={{flexDirection:'row'}}>
		 <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5 ,marginRight :15}}>Task Name:</Text>
		 <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5 ,marginRight :15}}>{movie.taskname}</Text>
		</View>
		<View  style={{flexDirection:'row'}}>
		 <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5 ,marginRight :15}}>Description:</Text>
		<Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5 ,marginRight :15}}>{movie.taskdetail}</Text>
		 
		</View>	
		<View  style={{flexDirection:'row'}}>
		 <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5 ,marginRight :15}}>Status</Text>
		 <Text style={{color:'#004D40',fontSize: 20,marginTop:5 ,alignSelf :'flex-start' ,marginBottom :5 ,marginRight :15}}>{movie.status}</Text>
		</View>	
		<View style={{backgroundColor:'#004D40' ,height:1}}></View>
		</View>
		  
		);
		
	  }
	  
	  
	  homeTask() {
		   ToastAndroid.show('home', ToastAndroid.SHORT)
		   this.setState({ type: 'home' })
		   console.log('mkmkkm')
		 //  this.state.type="home"
		  // this.render()
		 
	 } 
	 
	 ALLTask() {
		   ToastAndroid.show('all TASK', ToastAndroid.SHORT)
		   this.setState({ type: 'all' })
			 fetch("https://api.parse.com/1/classes/todo", {
	  headers: {
		"X-Parse-Application-Id": "Nlv14hb32fwBopAVXMzF6LRq1kom5TVC2PGOGxVt",
		"X-Parse-REST-API-Key": "tfkJINQzZWK8xRuWVK56UpJsyCVvKgwwTYHuWx56"
	  }
	})
	  .then((response) => response.json())
	  .then((responseData) => {

		  
		 this.setState({
		  dataSource: this.state.dataSource.cloneWithRows(responseData.results),
		  
	  })
	  
	  
	  })
	  .catch(function(error) {
		console.log(error)
	  })
	 .done();
		   
		// this.state.type='all'
		// this.render()
	 } 
	 PendingTask() {
		   ToastAndroid.show('pendingTask', ToastAndroid.SHORT)
		  
			this.setState({ type: 'pending' })
	fetch("https://api.parse.com/1/classes/todo", {
	  headers: {
		"X-Parse-Application-Id": "Nlv14hb32fwBopAVXMzF6LRq1kom5TVC2PGOGxVt",
		"X-Parse-REST-API-Key": "tfkJINQzZWK8xRuWVK56UpJsyCVvKgwwTYHuWx56"
	  }
	})
	  .then((response) => response.json())
	  .then((responseData) => {
		  
		  this.data = [];
			for (var i = 0; i < responseData.results.length ; ++i) {
				
				if( responseData.results[i].status=='pending' )
				this.data.push(responseData.results[i]);
			}
		  
		 this.setState({
		  dataSource: this.state.dataSource.cloneWithRows(this.data),
		  
	  })
	  

	  })
	  .catch(function(error) {
		console.log(error)
	  })
	 .done();
			
			
		  // this.state.type='pendig'
		  // this.render()
		 
	 } 
	 CompletedTask() {
		   ToastAndroid.show('pendingTask', ToastAndroid.SHORT)
			 this.setState({ type: 'completed' })
			   fetch("https://api.parse.com/1/classes/todo", {
	  headers: {
		"X-Parse-Application-Id": "Nlv14hb32fwBopAVXMzF6LRq1kom5TVC2PGOGxVt",
		"X-Parse-REST-API-Key": "tfkJINQzZWK8xRuWVK56UpJsyCVvKgwwTYHuWx56"
	  }
	})
	  .then((response) => response.json())
	  .then((responseData) => {
		  
		  this.data = [];
			for (var i = 0; i < responseData.results.length ; ++i) {
				
				if( responseData.results[i].status=='completed' )
				this.data.push(responseData.results[i]);
			}
		  
		 this.setState({
		  dataSource: this.state.dataSource.cloneWithRows(this.data),
		  
	  })
	  
	  
	  })
	  .catch(function(error) {
		console.log(error)
	  })
	 .done();
		  // this.state.type='completed'
		  // this.render()
		 
	 }

	}

	 
	const styles = StyleSheet.create({
	  container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	  },
	  plusimg :{
		width: 50,
		height :50,	
		marginRight : 5,
		marginTop : 25,
		alignSelf :"flex-end"
	  },

	 
	  welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	  },
	  instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	  },
	});

	AppRegistry.registerComponent('TodoApp', () => TodoApp);
